package io.titova.triangle;

public class App {
    public static void main(String[] args) {
        System.out.println("1 figure:");
        printFirstFigure(4);

        System.out.println("2 figure:");
        printSecondFigure(4);

        System.out.println("3 figure:");
        printThirdFigure(4);

        System.out.println("4 figure:");
        printFourthFigure(4);

        System.out.println("5 figure:");
        printFifthFigure(4);

        System.out.println("6 figure:");
        printSixthFigure(4);
    }

    private static void printFirstFigure(int size){
        leftDownTriangle(size);
        leftUpTriangle(size);
    }

    private static void printSecondFigure(int size){
        leftDownTriangle(size);
        rightUpTriangle(size);
    }

    private static void printThirdFigure(int size){
        rightDownTriangle(size);
        rightUpTriangle(size);
    }

    private static void printFourthFigure(int size){
        rightDownTriangle(size);
        leftUpTriangle(size);
    }

    private static void printFifthFigure(int size){
        rightDownTriangle(size);
        rectangle(size);
        rightUpTriangle(size);
    }

    private static void printSixthFigure(int size){
        rightDownTriangle(size);
        rectangle(size);
        leftUpTriangle(size);
    }

    private static void leftDownTriangle(int num) {
        for (int i = 0; i < num; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(".");
            }
            System.out.println();
        }
    }

    public static void leftUpTriangle(int num){
        for (int i = 0; i < num; i++) {
            for (int j = num-i; j > 0; j--) {
                System.out.print(".");
            }
            System.out.println();
        }
    }

    public static void rightUpTriangle(int num){
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                if (j>=i){
                    System.out.print(".");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void rightDownTriangle(int num){
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                if (j>=num-i-1){
                    System.out.print(".");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void rectangle(int num) {
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(".");
            }
            System.out.println();
        }
    }
}
